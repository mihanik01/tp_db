require 'test_helper'

class ForumControllerTest < ActionController::TestCase
  test "should get create" do
    get :create
    assert_response :success
  end

  test "should get details" do
    get :details
    assert_response :success
  end

  test "should get listPosts" do
    get :listPosts
    assert_response :success
  end

  test "should get listThreads" do
    get :listThreads
    assert_response :success
  end

  test "should get listUsers" do
    get :listUsers
    assert_response :success
  end

end
