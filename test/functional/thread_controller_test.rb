require 'test_helper'

class ThreadControllerTest < ActionController::TestCase
  test "should get close" do
    get :close
    assert_response :success
  end

  test "should get create" do
    get :create
    assert_response :success
  end

  test "should get details" do
    get :details
    assert_response :success
  end

  test "should get list" do
    get :list
    assert_response :success
  end

  test "should get listPosts" do
    get :listPosts
    assert_response :success
  end

  test "should get open" do
    get :open
    assert_response :success
  end

  test "should get remove" do
    get :remove
    assert_response :success
  end

  test "should get restore" do
    get :restore
    assert_response :success
  end

  test "should get subscribe" do
    get :subscribe
    assert_response :success
  end

  test "should get unsubscribe" do
    get :unsubscribe
    assert_response :success
  end

  test "should get update" do
    get :update
    assert_response :success
  end

  test "should get vote" do
    get :vote
    assert_response :success
  end

end
