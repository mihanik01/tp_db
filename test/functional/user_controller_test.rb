require 'test_helper'

class UserControllerTest < ActionController::TestCase
  test "should get create" do
    get :create
    assert_response :success
  end

  test "should get details" do
    get :details
    assert_response :success
  end

  test "should get follow" do
    get :follow
    assert_response :success
  end

  test "should get listFollowers" do
    get :listFollowers
    assert_response :success
  end

  test "should get listFollowing" do
    get :listFollowing
    assert_response :success
  end

  test "should get listPosts" do
    get :listPosts
    assert_response :success
  end

  test "should get unfollow" do
    get :unfollow
    assert_response :success
  end

  test "should get updateProfile" do
    get :updateProfile
    assert_response :success
  end

end
