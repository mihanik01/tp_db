class ApplicationController < ActionController::Base
  include Helper
  before_filter :setup
  after_filter :teardown

  def clear
    begin
      @client.query "DROP DATABASE tp_db;"
      @client.query "CREATE DATABASE tp_db;"
      @client.query "USE tp_db;"

      @client.query "CREATE TABLE user (
      about          TEXT,
      email          CHAR(30) UNIQUE,
      id             INT AUTO_INCREMENT, PRIMARY  KEY (id),
      isAnonymous    BOOL,
      name           CHAR(30),
      username       CHAR(30)
      );"

      @client.query"CREATE TABLE forum (
      id             INT AUTO_INCREMENT, PRIMARY KEY (id),
      name           CHAR(100) UNIQUE,
      short_name     CHAR(50) UNIQUE,
      creator        INT, FOREIGN KEY (creator) REFERENCES user (id)
      );"

      @client.query"CREATE TABLE thread (
      id             INT AUTO_INCREMENT, PRIMARY  KEY (id),
      forum_id       INT, FOREIGN KEY (forum_id) REFERENCES forum (id),
      isDeleted      BOOL,
      title          CHAR(100),
      isClosed       BOOL,
      creator_id     INT, FOREIGN KEY (creator_id) REFERENCES user (id),
      date           DateTime,
      message        TEXT,
      slug           TEXT,
      likes          INT NOT NULL DEFAULT 0,
      dislikes       INT NOT NULL DEFAULT 0,
      points         INT NOT NULL DEFAULT 0,
      posts          INT NOT NULL DEFAULT 0
      );"

      @client.query"CREATE TABLE post (
      id             INT AUTO_INCREMENT, PRIMARY KEY (id),
      parent_id      INT, FOREIGN KEY (parent_id) REFERENCES post (id),
      isApproved     BOOL NOT NULL DEFAULT false,
      isHighlighted  BOOL NOT NULL DEFAULT false,
      isEdited       BOOL NOT NULL DEFAULT false,
      isSpam         BOOL NOT NULL DEFAULT false,
      isDeleted      BOOL NOT NULL DEFAULT false,
      date           DateTime,
      thread_id      INT, FOREIGN KEY (thread_id) REFERENCES thread (id),
      message        TEXT,
      user_id        INT, FOREIGN KEY (user_id) REFERENCES user (id),
      forum_id       INT, FOREIGN KEY (forum_id) REFERENCES forum (id),
      likes          INT NOT NULL DEFAULT 0,
      dislikes       INT NOT NULL DEFAULT 0,
      points         INT NOT NULL DEFAULT 0,
      mpath          CHAR(128) DEFAULT ''
      );"

      @client.query"CREATE TABLE follow_relations (
      follower       INT, FOREIGN KEY (follower) REFERENCES user (id),
      followee       INT, FOREIGN KEY (followee) REFERENCES user (id)
      );"

      @client.query"CREATE TABLE subscript_relations (
      subscriber     INT, FOREIGN KEY (subscriber) REFERENCES user   (id),
      thread_id      INT, FOREIGN KEY (thread_id)  REFERENCES thread (id)
      );"

      @client.query "CREATE TABLE forum_to_user (
        forum_id INT, FOREIGN KEY (forum_id) REFERENCES forum (id),
        user_id INT, FOREIGN KEY (user_id) REFERENCES user (id)
      );"

      # Encoding
      @client.query "SET collation_connection = 'utf8_general_ci'"
      @client.query "ALTER DATABASE tp_db CHARACTER SET utf8 COLLATE utf8_general_ci"
      @client.query "ALTER TABLE forum convert to character set utf8 collate utf8_general_ci"
      @client.query "ALTER TABLE user convert to character set utf8 collate utf8_general_ci"
      @client.query "ALTER TABLE thread convert to character set utf8 collate utf8_general_ci"
      @client.query "ALTER TABLE post convert to character set utf8 collate utf8_general_ci"

      # Indexes
      # for user:
      @client.query "CREATE INDEX user_email_id
      ON user
      (email)"
      @client.query "CREATE INDEX user_id_email
      ON user
      (id, email)"

      @client.query "CREATE INDEX user_listPosts
      ON post
      (user_id, date)"
      @client.query "CREATE INDEX user_subscriptions
      ON subscript_relations
      (subscriber, thread_id)"

      # for forum:
      @client.query "CREATE INDEX forum_getShortName
      ON forum
      (id, short_name)"
      @client.query "CREATE INDEX forum_by_shortName
      ON forum
      (short_name)"

      @client.query "CREATE INDEX forum_listPosts
      ON post
      (forum_id, date, user_id)"
      @client.query "CREATE INDEX forum_listThreads
      ON thread
      (forum_id, date)"

      @client.query "CREATE INDEX forum_listUsers
      ON forum_to_user
      (forum_id, user_id)"

      #for post:
      @client.query "CREATE INDEX post_mpathID
      ON post
      (id, mpath)"

      #@client.query "SET GLOBAL key_buffer_size = 8388608"

     # @client.query "SET GLOBAL usercache.key_buffer_size = 1000000"
      #@client.query "CACHE INDEX user IN usercache;"

      #@client.query "SET GLOBAL forumcache.key_buffer_size = 1000000"
     # @client.query "CACHE INDEX forum IN forumcache"

     # @client.query "LOAD INDEX INTO CACHE user, forum"


      #disabling indexes
      #@client.query "ALTER TABLE user   DISABLE KEYS"
      #@client.query "ALTER TABLE post   DISABLE KEYS"
      #@client.query "ALTER TABLE thread DISABLE KEYS"
      #@client.query "ALTER TABLE forum  DISABLE KEYS"

      @client.query "SET FOREIGN_KEY_CHECKS = 0"
      @client.query "SET UNIQUE_CHECKS = 0"

     #response['Connection'] = 'keep-alive'
     render :json => {:code=>0, :response => "success"}
    rescue
     #response['Connection'] = 'keep-alive'
     render :json => {
        :code=>5,
        :response => "Internal database creation error"
      }
    end
  end

  def welcome
   #response['Connection'] = 'keep-alive'
     render :file => 'public/index.html', :layout=>false
  end

  def status
    code = 0;
    user = (@client.query "SELECT COUNT(*) AS user FROM user").first['user']
    thread = (@client.query "SELECT COUNT(*) AS thread FROM thread").first['thread']
    forum = (@client.query "SELECT COUNT(*) AS forum FROM forum").first['forum']
    post = (@client.query "SELECT COUNT(*) AS post FROM post").first['post']

   #response['Connection'] = 'keep-alive'
     render :json => {:code=>0, :response=>{:user=>user, :thread=>thread, :forum=>forum, :post=>post}}
  end
end
