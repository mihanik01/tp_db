class ThreadsController < ApplicationController
  include Helper
  before_filter :setup
  after_filter :teardown

  def close
    req = ActiveSupport::JSON.decode(request.body)
    thread = @client.escape(req['thread'])
    code = 0
    @client.query "UPDATE thread SET isClosed=true WHERE id='#{thread}'"
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => {:thread => thread}}
  end

  def create
    req = ActiveSupport::JSON.decode(request.body)
    forum = @client.escape(req['forum'])
    title = @client.escape(req['title'])
    isClosed = req['isClosed']
    user = @client.escape(req['user'])
    date = @client.escape(req['date'])
    message = @client.escape(req['message'])
    slug = @client.escape(req['slug'])
    isDeleted = false
    isDeleted = req['isDeleted'] if req.has_key? 'isDeleted'

    code = 0
    begin
      forum_id = get_forum_id forum
      user_id  = get_usr_id user
      @client.query "INSERT INTO thread SET
                      forum_id = '#{forum_id}',
                      isDeleted = #{isDeleted},
                      title = '#{title}',
                      isClosed = #{isClosed},
                      creator_id = '#{user_id}',
                      date = '#{date}',
                      message = '#{message}',
                      slug = '#{slug}';"
    rescue
    end
    thread = nil
    thread = @client.query("SELECT DATE_FORMAT(date, '%Y-%m-%d %H:%i:%s') AS date, forum.short_name AS forum,
        thread.id AS id, isClosed, isDeleted, message, slug, title,
        user.email AS user FROM thread JOIN forum JOIN user
        ON thread.creator_id = user.id AND thread.forum_id = forum.id
        WHERE thread.title = '#{title}'").first
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => thread}
  end

  def details
    thread = request.GET[:thread]
    results = @client.query "SELECT DATE_FORMAT(date, '%Y-%m-%d %H:%i:%s') AS date,
        id, isClosed, isDeleted, message, slug, title, likes, dislikes, points, forum_id, creator_id, posts FROM thread WHERE id = #{thread}"
    related = get_related request
    if results.count > 0 and not related.include? 'thread'
      result = results.first
      code = 0
      if related.include? 'user'
        result['user'] = get_usr_info result['creator_id']
      else
        result['user'] = get_usr_email result['creator_id']
      end
      if related.include? 'forum'
        result['forum'] = get_forum_info_id result['forum_id']
      else
        result['forum'] = get_forum_name result['forum_id']
      end
      result.except!('forum_id', 'user_id')
    else
      code = 3
      result = "Not found"
    end
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => result}
  end

  def list
    since = ""
    limit = 18446744073709551610
    order = 'desc'
    since = @client.escape(request.GET[:since]) if request.GET[:since]
    limit = @client.escape(request.GET[:limit]) if request.GET[:limit]
    order = @client.escape(request.GET[:order]) if request.GET[:order]

    since = " AND date >= '#{since}' " if since != ''
    limit = " LIMIT #{limit} "
    order.upcase!

    user, forum = nil
    user = @client.escape(request.GET[:user]) if request.GET[:user]
    forum = @client.escape(request.GET[:forum]) if request.GET[:forum]
    code = 0
    if forum
      arr = @client.query(
       "SELECT DATE_FORMAT(date, '%Y-%m-%d %H:%i:%s') AS date, forum.short_name AS forum,
        thread.id AS id, isClosed, isDeleted, likes, dislikes, points, posts,
        message, slug, title,
        user.email AS user FROM thread JOIN forum JOIN user
        ON thread.creator_id = user.id AND thread.forum_id = forum.id
        WHERE forum.short_name='#{forum}' #{since} ORDER BY date #{order} #{limit}")
    elsif user
      arr = @client.query(
       "SELECT DATE_FORMAT(date, '%Y-%m-%d %H:%i:%s') AS date, forum.short_name AS forum,
        thread.id AS id, isClosed, isDeleted, likes, dislikes, points, posts,
        message, slug, title,
        user.email AS user FROM thread JOIN forum JOIN user
        ON thread.creator_id = user.id AND thread.forum_id = forum.id
        WHERE user.email='#{user}' #{since} ORDER BY date #{order} #{limit}")
    end
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => arr}
  end

  def listPosts
    sort = 'flat'
    order = 'desc'
    limit = 18446744073709551610
    since = ''

    since = @client.escape(request.GET[:since]) if request.GET[:since]
    limit = @client.escape(request.GET[:limit]) if request.GET[:limit]
    order = @client.escape(request.GET[:order]) if request.GET[:order]

    since = " AND date >= '#{since}' " if since != ''
    limit = " LIMIT #{limit} "
    order.upcase!

    thread = request.GET[:thread]

    getter = "DATE_FORMAT(date, '%Y-%m-%d %H:%i:%s') AS date, dislikes, forum.short_name AS forum, post.id AS id,
              isApproved, isDeleted, isEdited, isHighlighted,
              isSpam, likes, message, parent_id AS parent, points, thread_id AS thread,
              user.email AS user FROM post JOIN forum JOIN user
              ON post.forum_id = forum.id AND post.user_id=user.id"

    if sort == 'flat'
      arr = @client.query(
      "SELECT #{getter} WHERE post.thread_id=#{thread} #{since}
       ORDER BY date #{order} #{limit}")
    elsif sort == 'tree'
      arr = @client.query(
      "SELECT #{getter} WHERE post.thread_id=#{thread} #{since}
       ORDER BY mpath ASC, date #{order} #{limit}")
    else
      arr = @client.query(
      "SELECT #{getter} JOIN (SELECT id FROM post WHERE post.thread_id=#{thread}
       AND mpath='' #{since} ORDER BY id #{limit}) t1 ON post.mpath='' OR
       post.mpath LIKE CONCAT('/',t1.id)+'*' ORDER BY mpath ASC, date #{order}")
    end
    code = 0
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => arr}
  end

  def open
    req = ActiveSupport::JSON.decode(request.body)
    code = 0
    thread = req['thread']
    @client.query "UPDATE thread SET isClosed = false WHERE id = '#{thread}'"
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => {:thread => thread}}
  end

  def close
    req = ActiveSupport::JSON.decode(request.body)
    code = 0
    thread = req['thread']
    @client.query "UPDATE thread SET isClosed = true WHERE id = '#{thread}'"
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => {:thread => thread}}
  end

  def remove
    req = ActiveSupport::JSON.decode(request.body)
    code = 0
    thread = req['thread']
    @client.query "UPDATE thread SET isDeleted = true, posts = 0 WHERE id = '#{thread}'"
    @client.query "UPDATE post SET isDeleted = true WHERE thread_id = '#{thread}'"
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => {:thread => thread}}
  end

  def restore
    req = ActiveSupport::JSON.decode(request.body)
    code = 0
    thread = req['thread']
    @client.query "UPDATE thread SET isDeleted = false, posts =
                    (SELECT COUNT(id) FROM post
                     WHERE thread_id = #{thread}) WHERE id = '#{thread}'"
    @client.query "UPDATE post   SET isDeleted = false WHERE thread_id = '#{thread}'"
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => {:thread => thread}}
  end

  def subscribe
    req = ActiveSupport::JSON.decode(request.body)
    code = 0
    thread = req['thread']
    email  = req['user']
    user   = get_usr_id email
    @client.query "INSERT IGNORE INTO subscript_relations SET
                    thread_id='#{thread}', subscriber='#{user}'"
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => {:thread => thread, :user=>email}}
  end

  def unsubscribe
    req = ActiveSupport::JSON.decode(request.body)
    code = 0
    thread = req['thread']
    email  = req['user']
    user   = get_usr_id email
    @client.query "DELETE FROM subscript_relations WHERE
                    thread_id='#{thread}' AND subscriber='#{user}'"
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => {:thread => thread, :user=>email}}
  end

  def update
    req = ActiveSupport::JSON.decode(request.body)
    code = 0
    message = @client.escape(req['message'])
    slug = @client.escape(req['slug'])
    thread = req['thread']

    @client.query "UPDATE thread SET message='#{message}', slug='#{slug}'
                    WHERE id='#{thread}'"
    thread = (@client.query "SELECT DATE_FORMAT(date, '%Y-%m-%d %H:%i:%s') AS date, forum.short_name AS forum,
        thread.id AS id, isClosed, isDeleted, message, slug, title,
        user.email AS user FROM thread JOIN forum JOIN user
        ON thread.creator_id = user.id AND thread.forum_id = forum.id
        WHERE thread.id = #{thread}").first
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => thread}
  end

  def vote
    req = ActiveSupport::JSON.decode(request.body)
    vote = req['vote']
    thread = req['thread']
    @client.query "UPDATE thread SET
                    likes    = CASE WHEN #{vote}=1 THEN likes+1  ELSE likes      END,
                    dislikes = CASE WHEN #{vote}=1 THEN dislikes ELSE dislikes+1 END,
                    points = likes-dislikes
                    WHERE id = '#{thread}'"
    code = 0
    thread = (@client.query "SELECT DATE_FORMAT(date, '%Y-%m-%d %H:%i:%s') AS date, forum.short_name AS forum,
        thread.id AS id, isClosed, isDeleted, message, slug, title, likes, dislikes, points,
        user.email AS user FROM thread JOIN forum JOIN user
        ON thread.creator_id = user.id AND thread.forum_id = forum.id
        WHERE thread.id = #{thread}").first
    if thread.nil?
      code = 1
      thread = "Not found"
    end
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => thread}
  end
end
