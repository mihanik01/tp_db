class PostsController < ApplicationController
  include Helper
  before_filter :setup
  after_filter :teardown

  skip_before_filter :setup, :only => [:create]
  skip_after_filter  :teardown, :only => [:create]

  def create
    begin
      client = Mysql2::Client.new(:host => 'localhost',
                                    :database => 'tp_db',
                                    :username => 'root',
                                    :password => '0000')
      begin
        req = ActiveSupport::JSON.decode(request.body)
        date    = client.escape(req['date'])
        thread  = req['thread']
        user    = client.escape(req['user'])
        forum   = client.escape(req['forum'])
        parent  = "null"
        parent         = req['parent'].to_s if req.has_key? 'parent' and not req['parent'].nil?
        isApproved     = defbool(req, 'isApproved')
        isHighlighted  = defbool(req, 'isHighlighted')
        isEdited       = defbool(req, 'isEdited')
        isSpam         = defbool(req, 'isSpam')
        isDeleted      = defbool(req, 'isDeleted')

        user_id = forum_id = mpath = nil
        begin
          user_id = client.query("SELECT id FROM user WHERE email = '#{user}'").first['id']
          forum_id = client.query("SELECT id FROM forum WHERE short_name = '#{forum}'").first['id']

          mpath = ''

          parentMpath = ''
          parentMpath = client.query("SELECT mpath FROM post WHERE id = '#{req['parent']}'").first['mpath'] if parent != 'null'
          mpath = "#{parentMpath}/#{parent.rjust(8,"0")}" if parent != 'null'

          ok = false
          while !ok do
            client.query "START TRANSACTION"
            begin
              client.query "INSERT INTO post SET
                            parent_id     = #{parent},
                            isApproved    = #{isApproved},
                            isHighlighted = #{isHighlighted},
                            isEdited      = #{isEdited},
                            isSpam        = #{isSpam},
                            isDeleted     = #{isDeleted},
                            date = '#{date}',
                            thread_id = '#{thread}',
                            message = '#{req['message']}',
                            user_id = '#{user_id}',
                            forum_id = '#{forum_id}',
                            mpath = '#{mpath}'"
              code = 0
              req['id'] = (client.query "SELECT last_insert_id() AS id").first['id']

              client.query "SELECT posts FROM thread WHERE id = #{thread} LOCK IN SHARE MODE"
              client.query "UPDATE thread SET posts = posts + 1 WHERE id = #{thread}"
              client.query "COMMIT"
              ok = true
            rescue Exception => e
              code = 4
              req = e.message
              client.query "ROLLBACK"
            end
          end
        rescue Exception => e
          code = 4.5
          req = e.message
        end
      rescue
        code = 2
        req = 'Invalid JSON'
      end
      client.close
    rescue
      code = 4
      req  = "MySQL error"
    end
    render :json => {:code => code, :response => req}
  end

  def details
    post_id = request.GET[:post]
    related = get_related request

    results = @client.query("
    SELECT
    DATE_FORMAT(post.date, '%Y-%m-%d %H:%i:%s') AS date,
    id,
    isApproved,
    isDeleted,
    isEdited,
    isHighlighted,
    isSpam,
    message,
    parent_id,
    thread_id,
    forum_id,
    user_id,
    likes,
    dislikes,
    points
    FROM post
    WHERE id='#{post_id}'")
    if results.count > 0 then
      code = 0
      post = results.first
      forum_id  = post['forum_id']
      parent_id = post['parent_id']
      thread_id = post['thread_id']
      user_id   = post['user_id']

      post.except!('forum_id', 'parent_id', 'thread_id', 'user_id')
      if related.include? 'forum'
        post['forum'] = get_forum_info_id forum_id
      else
        post['forum'] = get_forum_name forum_id
      end
      post['parent'] = parent_id
      if related.include? 'thread'
        post['thread'] = get_thread_info_by_id thread_id
      else
        post['thread'] = thread_id
      end
      if related.include? 'user'
        post['user'] = get_usr_info_short user_id
      else
        post['user'] = get_usr_email user_id
      end
    else
      code = 1
      post = "Not found"
    end
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => post}
  end

  def list
    forum, thread = nil
    forum = @client.escape(request.GET[:forum]) if request.GET[:forum]
    thread = @client.escape(request.GET[:thread]) if request.GET[:thread]
    since = ""
    limit = 18446744073709551610
    order = 'desc'
    since = @client.escape(request.GET[:since]) if request.GET[:since]
    limit = request.GET[:limit] if request.GET[:limit]
    order = @client.escape(request.GET[:order]) if request.GET[:order]

    since = " AND date >= '#{since}' " if since != ''
    limit = " LIMIT #{limit} "
    order.upcase!

    code = 0
    if forum
      arr = @client.query(
      "SELECT DATE_FORMAT(post.date, '%Y-%m-%d %H:%i:%s') AS date, dislikes, forum.short_name AS forum,
       post.id AS id, isApproved, isDeleted, isEdited, isHighlighted,
       isSpam, likes, message, parent_id AS parent, points, thread_id AS thread,
       user.email AS user FROM post JOIN forum JOIN user ON
       post.forum_id = forum.id AND post.user_id = user.id
       WHERE forum.short_name='#{forum}' #{since} ORDER BY date #{order} #{limit}")
    elsif thread
      arr = @client.query(
      "SELECT DATE_FORMAT(post.date, '%Y-%m-%d %H:%i:%s') AS date, dislikes, forum.short_name AS forum,
       post.id AS id, isApproved, isDeleted, isEdited, isHighlighted,
       isSpam, likes, message, parent_id AS parent, points, thread_id AS thread,
       user.email AS user FROM post JOIN forum JOIN user ON
       post.forum_id = forum.id AND post.user_id = user.id
       WHERE post.thread_id='#{thread}' #{since} ORDER BY date #{order} #{limit}")
    end
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => arr}
  end

  def remove
    req = ActiveSupport::JSON.decode(request.body)
    code = 0
    post = req['post']
    @client.query "UPDATE post SET isDeleted = true WHERE id = '#{post}'"
    thread = (@client.query "SELECT thread_id FROM post WHERE id = '#{post}'").first['thread_id']
    @client.query "UPDATE thread SET posts = posts - 1 WHERE id = #{thread}"
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => {:post => post}}
  end

  def restore
    req = ActiveSupport::JSON.decode(request.body)
    code = 0
    post = req['post']
    @client.query "UPDATE post SET isDeleted = false WHERE id = '#{post}'"
    thread = (@client.query "SELECT thread_id FROM post WHERE id = '#{post}'").first['thread_id']
    @client.query "UPDATE thread SET posts = posts + 1 WHERE id = #{thread}"
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => {:post => post}}
  end

  def update
    req = ActiveSupport::JSON.decode(request.body)
    post = req['post']
    message = @client.escape(req['message'])
    code = 0
    @client.query "UPDATE post SET message='#{message}' WHERE id = '#{post}'"
    post = @client.query(
      "SELECT DATE_FORMAT(post.date, '%Y-%m-%d %H:%i:%s') AS date, dislikes, forum.short_name AS forum,
       post.id AS id, isApproved, isDeleted, isEdited, isHighlighted,
       isSpam, likes, message, parent_id AS parent, points, thread_id AS thread,
       user.email AS user FROM post JOIN forum JOIN user ON
       post.forum_id = forum.id AND post.user_id = user.id
       WHERE post.id = '#{post}'").first
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => post}
  end

  def vote
    req = ActiveSupport::JSON.decode(request.body)
    vote = req['vote']
    post = req['post']
    @client.query "UPDATE post SET
                    likes    = CASE WHEN #{vote}=1 THEN likes+1  ELSE likes      END,
                    dislikes = CASE WHEN #{vote}=1 THEN dislikes ELSE dislikes+1 END,
                    points = likes-dislikes
                    WHERE id = '#{post}'"
    code = 0
    post = @client.query(
      "SELECT DATE_FORMAT(post.date, '%Y-%m-%d %H:%i:%s') AS date, dislikes, forum.short_name AS forum,
       post.id AS id, isApproved, isDeleted, isEdited, isHighlighted,
       isSpam, likes, message, parent_id AS arent, points, thread_id AS thread,
       user.email AS user FROM post JOIN forum JOIN user ON
       post.forum_id = forum.id AND post.user_id = user.id
       WHERE post.id = '#{post}'")
    if post.nil?
      code = 1
      post = "Not found"
    end
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => post}
  end
end
