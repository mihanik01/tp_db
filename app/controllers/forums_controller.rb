# -*- coding: utf-8 -*-

class ForumsController < ApplicationController
  include Helper
  before_filter :setup
  after_filter :teardown

  def create
    req = ActiveSupport::JSON.decode(request.body)

    req['name']       = @client.escape(req['name'])
    req['short_name'] = @client.escape(req['short_name'])
    req['user']       = @client.escape(req['user'])
    begin
      user_id = get_usr_id req['user']

        @client.query "INSERT INTO forum SET
                        name       = '#{req['name']      }',
                        short_name = '#{req['short_name']}',
                        creator    = '#{user_id          }'"
        id = (@client.query "SELECT last_insert_id() AS id").first['id']

      code = 0
      obj  = {:id => id,
              :name => req['name'],
              :user => req['user'],
              :short_name => req['short_name']}
    rescue
      code = 5
      obj = "Unknown error"
    end
    render :json => {:code => code, :response => obj}
  end

  def details
    related    = (get_related request).include? 'user'
    short_name = request.GET['forum']

    begin
      q = @client.query("SELECT * FROM forum
                          WHERE short_name = '#{short_name}'")
      if q.count == 1 then
        forum = q.first
        forum['user'] = get_usr_info forum['creator'] if related
        forum.except!('creator')
        code = 0
        obj = forum
      else
        code = 1
        obj = {:status=>'Not found',:shname=>short_name, :q=>q}
      end
    rescue
      code = 5
      obj = "Unknown error"
    end
   render :json => {:code => code, :response => obj}
  end

  def listPosts
    short_name = @client.escape(request.GET['forum'])
    limit = 18446744073709551610
    order = 'desc'
    since = ''
    related = []

    limit   = request.GET['limit'] if request.GET['limit']
    order   = @client.escape(request.GET['order']) if request.GET['order']
    since   = @client.escape(request.GET['since']) if request.GET['since']
    related = get_related request

    since = " AND date >= '#{since}' " if since != ''
    limit = " LIMIT #{limit} "
    order.upcase!

    begin
      forum_id = get_forum_id short_name

      posts = @client.query("SELECT
      DATE_FORMAT(post.date, '%Y-%m-%d %H:%i:%s') AS date,
      id,
      isApproved,
      isDeleted,
      isEdited,
      isHighlighted,
      isSpam,
      message,
      parent_id AS parent,
      thread_id AS thread,
      user_id   AS user,
      likes,
      dislikes,
      points,
      '#{short_name}' AS forum
      FROM post
      WHERE forum_id='#{forum_id}'
      #{since} ORDER BY date
      #{order} #{limit}").map do |post|
        post['forum'] = get_forum_info_id forum_id if related.include? 'forum'

        post['thread'] = get_thread_info_by_id post['thread'] if related.include? 'thread'

        if related.include? 'user'
          post['user'] = get_usr_info_short post['user']
        else
          post['user'] = get_usr_email post['user']
        end
        post
      end

      code = 0
      response = posts
    rescue
      code = 1
      response  = "Not found"
    end
   render :json => {:code => code, :response =>response}
  end

  def listThreads
    short_name = @client.escape(request.GET['forum'])
    limit = 18446744073709551610
    order = 'desc'
    since = ''
    related = []

    limit   = request.GET['limit'] if request.GET['limit']
    order   = @client.escape(request.GET['order']) if request.GET['order']
    since   = @client.escape(request.GET['since']) if request.GET['since']
    related = get_related request

    since = " AND date >= '#{since}' " if since != ''
    limit = " LIMIT #{limit} "
    order.upcase!

    begin
      forum_id = get_forum_id short_name
      forum_info = get_forum_info short_name if related.include? 'forum'
      threads = @client.query("SELECT
        DATE_FORMAT(date, '%Y-%m-%d %H:%i:%s') AS date,
        id,
        isClosed,
        isDeleted,
        message,
        slug,
        title,
        likes,
        dislikes,
        points,
        '#{short_name}' AS forum,
        creator_id AS user,
        posts
        FROM thread
        WHERE forum_id = '#{forum_id}'
        #{since} ORDER BY date #{order} #{limit}").map do |thread|
          thread['forum'] = forum_info if related.include? 'forum'

          if related.include? 'user'
            thread['user'] = get_usr_info thread['user']
          else
            thread['user'] = get_usr_email thread['user']
          end
          thread
        end
      code = 0
    rescue
      code = 1
      threads  = "Not found"
    end
    render :json => {:code => code, :response => threads}
  end

  def listUsers
    short_name = @client.escape(request.GET['forum'])
    limit = 18446744073709551610
    order = 'desc'
    since = ''

    limit   = request.GET['limit'] if request.GET['limit']
    order   = @client.escape(request.GET['order']) if request.GET['order']
    since   = request.GET['since_id'] if request.GET['since_id']

    since = " AND user_id >= '#{since}' " if since != ''
    limit = " LIMIT #{limit} "
    order.upcase!

    begin
      forum_id = get_forum_id short_name
      users = @client.query("
        SELECT
        id,
        about,
        email,
        isAnonymous,
        name,
        username
        FROM user
        WHERE id IN
        (SELECT
        user_id
        FROM post
        WHERE forum_id = '#{forum_id}'
        #{since})
        ORDER BY name #{order} #{limit}").map do |usr|
          usr['followers'] = (get_followers usr['id']).map{|fol| fol['email']}
          usr['following'] = (get_following usr["id"]).map{|fol| fol['email']}
          usr['subscriptions'] = (get_subscriptions usr["id"]).map{|sub| sub['thread_id']}
          usr
        end
      code = 0
      resp = users
    rescue
      code = 1
      resp  = "Not found"
    end
    render :json => {:code => code, :response => resp}
  end
end
