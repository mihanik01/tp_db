class UsersController < ApplicationController
  include Helper
  before_filter :setup
  after_filter :teardown

  def create
    usr = ActiveSupport::JSON.decode(request.body)
    usr['isAnonymous'] = false if not usr.has_key? "isAnonymous"

    begin
      username = about = name = email = "null"
      username = "'#{@client.escape(usr["username"])}'" if usr["username"]
      about    = "'#{@client.escape(usr["about"])}'"    if usr["about"]
      name     = "'#{@client.escape(usr["name"])}'"     if usr["name"]
      email    = "'#{@client.escape(usr["email"])}'"    if usr["email"]
      cnt = (@client.query "SELECT 1 FROM user WHERE email = #{email}").count
      if cnt == 0 then
        @client.query "INSERT INTO user SET
                      username = #{username},
                      email = #{email},
                      about = #{about},
                      name  = #{name},
                      isAnonymous=#{usr['isAnonymous']}"
        usr['id'] = (@client.query "SELECT last_insert_id() AS id").first['id']
        code = 0
        resp = usr
      else
        code = 5
        resp = "Error: user already exists!"
        logger.error#response
      end
    rescue
      code = 3
      resp = "Incorrect query"
      logger.error#response
    end

    result = {:code => code, :response => resp}
   #response['Connection'] = 'keep-alive'
     render :json => result

  end

  def details
    usr_email = @client.escape(request.GET['user'])
    q_result = @client.query("SELECT * FROM user WHERE email='#{usr_email}'")
    if q_result.count == 0
      code = 1
      resp  = "Not found"
    else
      code = 0
      usr = q_result.first
      usr['followers'] = (get_followers usr['id']).map{|fol| fol['email']}
      usr['following'] = (get_following usr["id"]).map{|fol| fol['email']}
      usr['subscriptions'] = (get_subscriptions usr["id"]).map{|sub| sub['thread_id']}
      resp = usr
    end
    result = {:code => code, :response => resp}
   #response['Connection'] = 'keep-alive'
     render :json => result
  end

  def follow
    req = ActiveSupport::JSON.decode(request.body)
    req['follower'] = @client.escape(req['follower'])
    req['followee'] = @client.escape(req['followee'])
    fwer     = @client.query("SELECT id FROM user
                              WHERE email = '#{req['follower']}'")
                            .first
    fwee_id  = @client.query("SELECT id FROM user
                              WHERE email = '#{req['followee']}'")
                            .first['id']
    begin
      @client.query("INSERT INTO follow_relations SET
                      follower = '#{fwer['id']}',
                      followee = '#{fwee_id   }'")
      code = 0
      fwer['followers'] = (get_followers fwer['id']).map{|fol| fol['email']}
      fwer['following'] = (get_following fwer["id"]).map{|fol| fol['email']}
      fwer['subscriptions'] = (get_subscriptions fwer["id"]).map{|sub| sub['thread_id']}
      result = fwer
    rescue
      code   = 0
      result = @client.query("SELECT * FROM follow_relations
                            WHERE follower = '#{fwer['id']}' AND
                            followee = '#{fwee_id}'")
    end
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => result}
  end

  def listFollowers
    email = @client.escape(request.GET['user'])
    limit = 18446744073709551610
    order = 'desc'
    since = ''

    limit = request.GET['limit'] if request.GET['limit']
    order = @client.escape(request.GET['order']) if request.GET['order']
    since = @client.escape(request.GET['since']) if request.GET['since']

    since = " OFFSET #{since} " if since != ''
    limit = " LIMIT #{limit} "
    order.upcase!

    q_result = @client.query("SELECT * FROM user WHERE email='#{email}'")
    if q_result.count == 0
      code = 1
      resp  = "Not found"
    else
      code = 0
      usr = q_result.first
      fwer = @client.query(
      "SELECT about, email, id, isAnonymous, name, username FROM
       user JOIN follow_relations AS t1 ON user.id = t1.follower
       WHERE followee = #{usr["id"]}").map do |usr|
        usr['followers'] = (get_followers usr['id']).map{|fol| fol['email']}
        usr['following'] = (get_following usr["id"]).map{|fol| fol['email']}
        usr['subscriptions'] = (get_subscriptions usr["id"]).map{|sub| sub['thread_id']}
        usr
      end
      resp = fwer
    end
    result = {:code => code, :response => resp}
   #response['Connection'] = 'keep-alive'
     render :json => result
  end

  def listFollowing
    email = @client.escape(request.GET['user'])
    limit = 18446744073709551610
    order = 'desc'
    since = ''

    limit = request.GET['limit'] if request.GET['limit']
    order = @client.escape(request.GET['order']) if request.GET['order']
    since = @client.escape(request.GET['since']) if request.GET['since']

    since = " OFFSET #{since} " if since != ''
    limit = " LIMIT #{limit} "
    order.upcase!

    q_result = @client.query("SELECT * FROM user WHERE email='#{email}'")
    if q_result.count == 0
      code = 1
      response  = "Not found"
    else
      code = 0
      usr = q_result.first
      fwing = @client.query(
      "SELECT about, email, id, isAnonymous, name, username FROM
       user WHERE id IN
       (SELECT followee
       FROM follow_relations
       WHERE follower = #{usr["id"]})").map do |usr|
        usr['followers'] = (get_followers usr['id']).map{|fol| fol['email']}
        usr['following'] = (get_following usr["id"]).map{|fol| fol['email']}
        usr['subscriptions'] = (get_subscriptions usr["id"]).map{|sub| sub['thread_id']}
        usr
      end
     response = fwing
    end
    result = {:code => code, :response =>response}
   #response['Connection'] = 'keep-alive'
     render :json => result
  end

  def listPosts
    email = @client.escape(request.GET['user'])
    limit = 18446744073709551610
    order = 'desc'
    since = ''

    limit = request.GET['limit'] if request.GET['limit']
    order = @client.escape(request.GET['order']) if request.GET['order']
    since = @client.escape(request.GET['since']) if request.GET['since']

    since = " AND post.date >= '#{since}' " if since != ''
    limit = " LIMIT #{limit} "
    order.upcase!

    q_result = @client.query("SELECT email, id FROM user WHERE email='#{email}'")
    if q_result.count == 0
      code = 1
      resp  = "Not found"
    else
      code = 0
      usr = q_result.first

      posts = @client.query("SELECT
      DATE_FORMAT(post.date, '%Y-%m-%d %H:%i:%s') AS date,
      dislikes,
      forum_id AS forum,
      id,
      isApproved,
      isDeleted,
      isEdited,
      isHighlighted,
      isSpam,
      likes,
      message,
      parent_id AS parent,
      points,
      thread_id AS thread,
      '#{usr['email']}' AS user
      FROM post
      WHERE post.user_id = #{usr['id']}
      #{since} ORDER BY post.date #{order} #{limit}").map do |post|
        post['forum'] = get_forum_name post['forum']
        post
      end

      resp = posts
    end
    result = {:code => code, :response => resp}
   #response['Connection'] = 'keep-alive'
     render :json => result
  end

  def unfollow
    req = ActiveSupport::JSON.decode(request.body)
    req['follower'] = @client.escape(req['follower'])
    req['followee'] = @client.escape(req['followee'])
    fwer     = @client.query("SELECT id FROM user
                              WHERE email = '#{req['follower']}'")
                            .first
    fwee_id  = @client.query("SELECT id FROM user
                              WHERE email = '#{req['followee']}'")
                            .first['id']
    begin
      @client.query("DELETE FROM follow_relations WHERE
                      follower = '#{fwer['id']}' AND
                      followee = '#{fwee_id   }'")
      code = 0
      fwer['followers'] = (get_followers fwer['id']).map{|fol| fol['email']}
      fwer['following'] = (get_following fwer["id"]).map{|fol| fol['email']}
      fwer['subscriptions'] = (get_subscriptions fwer["id"]).map{|sub| sub['thread_id']}
      result = fwer
    rescue
      code   = 0
      result = @client.query("SELECT * FROM follow_relations
                            WHERE follower = '#{fwer['id']}' AND
                            followee = '#{fwee_id}'")
    end
   #response['Connection'] = 'keep-alive'
     render :json => {:code => code, :response => result}
  end

  def updateProfile
    begin
      usr = ActiveSupport::JSON.decode(request.body)
      usr["about"]    = @client.escape(usr["about"])
      usr["name"]     = @client.escape(usr["name"])
      usr["user"]     = @client.escape(usr["user"])
      begin
        @client.query "UPDATE user SET
                        about='#{usr['about']}',
                        name ='#{usr['name'] }'
                        WHERE email='#{usr['user']}'"
        usr['email'] = usr['user']
        usr.except! 'user'

        data = @client.query("SELECT id, isAnonymous, username FROM user
                              WHERE email = '#{usr['email']}'").first
        usr['id'] = data['id']
        usr['isAnonymous'] = data['isAnonymous']
        usr['username'] = data['username']
        usr['followers'] = (get_followers usr['id']).map{|fol| fol['email']}
        usr['following'] = (get_following usr["id"]).map{|fol| fol['email']}
        usr['subscriptions'] = (get_subscriptions usr["id"]).map{|sub| sub['thread_id']}
        code = 0
        resp = usr
      rescue
        code = 5
        resp = "Error: user already exists!"
      end
    rescue
      code = 2
      resp = "Invalid query"
    end
    result = {:code => code, :response => resp}
   #response['Connection'] = 'keep-alive'
     render :json => result
  end

end
