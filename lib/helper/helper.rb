module Helper
      def setup
        @client = Mysql2::Client.new(:host => 'localhost',
                                  :database => 'tp_db',
                                  :username => 'root',
                                  :password => '0000')
      end

      def teardown
        @client.close
      end

      def get_followers id
        @client.query("
          SELECT email FROM follow_relations JOIN user
          ON user.id = follower
          WHERE followee = '#{id}'")
      end

      def get_following id
        @client.query("
          SELECT email FROM follow_relations JOIN user
          ON user.id = followee
          WHERE follower = '#{id}'")
      end

      def get_subscriptions id
        @client.query("
          SELECT thread_id FROM subscript_relations
          WHERE subscriber = '#{id}'")
      end

      def get_usr_info id
        usr = @client.query("SELECT * FROM user WHERE id='#{id}'").first
        usr['followers'] = (get_followers usr['id']).map{|fol| fol['email']}
        usr['following'] = (get_following usr["id"]).map{|fol| fol['email']}
        usr['subscriptions'] = (get_subscriptions usr["id"]).map{|sub| sub['thread_id']}
        usr
      end

      def get_usr_info_short id
        @client.query("SELECT * FROM user WHERE id='#{id}'").first
      end

      def get_usr_name id
        @client.query("SELECT name FROM user WHERE id='#{id}'").first['name']
      end

      def get_usr_id email
        @client.query("SELECT id FROM user WHERE email = '#{email}'").first['id']
      end

      def get_usr_email id
        q = @client.query("SELECT email FROM user WHERE id='#{id}'")
        if q.count>0
         q.first['email']
        else
          ''
        end
      end

      def get_forum_info short_name
        forum = @client.query("SELECT * FROM forum
                                WHERE short_name = '#{short_name}'").first
        raise "Not found" if forum == nil
        forum['user'] = get_usr_email forum['creator']
        forum.except!('creator')
      end

      def get_forum_info_id id
        forum = @client.query("SELECT * FROM forum
                                WHERE id = '#{id}'").first
        raise "Not found" if forum == nil
        forum['user'] = get_usr_email forum['creator']
        forum.except!('creator')
      end

       def get_forum_name id
        @client.query("SELECT short_name FROM forum
                        WHERE id = '#{id}'").first['short_name']
      end

      def get_forum_id short_name
        @client.query("SELECT id FROM forum
                        WHERE short_name = '#{short_name}'").first['id']
      end

      def get_thread_info_by_id id
        results = @client.query "SELECT DATE_FORMAT(date, '%Y-%m-%d %H:%i:%s') AS date,
        id, isClosed, isDeleted, message, slug, title, forum_id, creator_id, posts, likes, dislikes, points FROM thread WHERE id = #{id}"

        if results.count > 0
          result = results.first
          result['user'] = get_usr_email result['creator_id']
          result['forum'] = get_forum_name result['forum_id']
          result.except!('forum_id', 'creator_id')
        else
          result = "Not found"
        end
        result
      end

      def get_related req
        req.original_url.scan(/[^?]elated=([^&]*)/).flatten
      end

      def defbool req, name
        return req[name] if req.has_key? name
        false
      end

      def defempty req, name
        return req[name] if req.has_key? name
        nil
      end
end
