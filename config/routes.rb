TpMysqlApi::Application.routes.draw do
  match  "db/api/clear",  to: 'application#clear'
  get   "db/api/status", to: 'application#status'
  root :to => "application#welcome"

  post  "db/api/forum/create", to: 'forums#create'

  post  "db/api/post/create", to: 'posts#create'
  post  "db/api/post/remove", to: 'posts#remove'
  post  "db/api/post/restore", to: 'posts#restore'
  post  "db/api/post/update", to: 'posts#update'
  post  "db/api/post/vote", to: 'posts#vote'

  post  "db/api/user/create", to: 'users#create'
  post  "db/api/user/follow", to: 'users#follow'
  post  "db/api/user/unfollow", to: 'users#unfollow'
  post  "db/api/user/updateProfile", to: 'users#updateProfile'

  post  "db/api/thread/close", to: 'threads#close'
  post  "db/api/thread/create", to: 'threads#create'
  post  "db/api/thread/details", to: 'threads#details'
  post  "db/api/thread/open", to: 'threads#open'
  post  "db/api/thread/remove", to: 'threads#remove'
  post  "db/api/thread/restore", to: 'threads#restore'
  post  "db/api/thread/subscribe", to: 'threads#subscribe'
  post  "db/api/thread/unsubscribe", to: 'threads#unsubscribe'
  post  "db/api/thread/update", to: 'threads#update'
  post  "db/api/thread/vote", to: 'threads#vote'

  get "db/api/user/:action", to: "users#:action"
  get "db/api/thread/:action", to: "threads#:action"
  get "db/api/post/:action", to: "posts#:action"
  get "db/api/forum/:action", to: "forums#:action"
end
